package ajaymehta.biggestsmallestelementinarray;

/**
 * Created by Avi Hacker on 7/10/2017.
 */

// Find Biggest and the Smallest element in Array..

public class Program1 {

    int smaller;  // we shoudnt took 0 value for smaller...coz there could be chances that our array elemements are bigger than 0..so u can put like 1k or 1lakh ..but better to put any array element...randomly ..before starting the loop..
    int bigger = 0;  // took initial values for these variables..

    public void find(int arr[]){

        smaller = arr[5];  // for the smaller number we put any randomly element from array ..we didnt took any value from our side..its better to any value from array then we gonna comare its value by all other values from array..

        for(int x : arr) {

           if(x > bigger) { // if any array element is bigger than element in bigger variable ..just override..

               bigger = x;
           }


            if(x <smaller) {  // if any array element is smaller than element in variable (smaller ) then override variable value..

                smaller = x;
            }

        }
        System.out.println("The biggest element of array is :"+bigger);
        System.out.println("The smallest element of array is :"+smaller);
        System.out.println();
        System.out.println("============================================");
        System.out.println();

    }


    public static void main(String args[]) {

        Program1 program1 = new Program1();

        program1.find(new int[]{5,23,6,86,12,33,3,9});

        program1.find(new int[]{3,5,9,5,34,8,8,4,223,8,95,3,1,1,3,7,8});

    }
}
